/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.CashTillNoPolymorphism;

/**
 *
 * @author rod
 */
public class CashTill {
    
    private Double runningTotal;
    
    public CashTill() {
        runningTotal = 0.0;
    }
    
    // If we didn't have polymorphism we would have to implement separate
    // methods for each different type of publication
    public void sellItem(Book b) {
        runningTotal += b.getPrice();
        
        b.sellCopy();
        
        System.out.println("Sold " + b + " @ " + b.getPrice());
        System.out.println("Subtotal = " + runningTotal);
    }
    
    public void sellItem(Magazine m) {
        runningTotal += m.getPrice();
        
        m.sellCopy();
        
        System.out.println("Sold " + m + " @ " + m.getPrice());
        System.out.println("Subtotal = " + runningTotal);
    }
    
    public void sellItem(DiscMag dm) {
        runningTotal += dm.getPrice();
        
        dm.sellCopy();
        
        System.out.println("Sold " + dm + " @ " + dm.getPrice());
        System.out.println("Subtotal = " + runningTotal);
    }
    
    public void showTotal() {
        System.out.println("GRAND TOTAL: " + runningTotal);
    }
}
