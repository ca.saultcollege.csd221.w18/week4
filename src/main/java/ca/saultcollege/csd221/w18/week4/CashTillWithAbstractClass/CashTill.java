/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.CashTillWithAbstractClass;

/**
 *
 * @author rod
 */
public class CashTill {
    
    private Double runningTotal;
    
    public CashTill() {
        runningTotal = 0.0;
    }
    
    // With polymorphism, we only need this one method that accepts a 
    // Publication.  Now, any subclass of this abstract class can be passed in
    // and the method will work just fine.  This will even work for new classes
    // that are added in the future!  We won't need to change this method!
    public void sellItem(Publication p) {
        runningTotal += p.getPrice();
        
        p.sellCopy();
        
        System.out.println("Sold " + p + " @ " + p.getPrice());
        System.out.println("Subtotal = " + runningTotal);
    }
    
    public void showTotal() {
        System.out.println("GRAND TOTAL: " + runningTotal);
    }
}
