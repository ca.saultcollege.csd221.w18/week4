/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.CashTillWithInterface;

import java.util.Date;

/**
 *
 * @author rod
 */
public class Magazine extends Publication {
    private Integer orderQty;
    private String currIssue;
    

    public Magazine(String title, Integer price, int orderQty, String currIssue, Integer copies) {
        super(title, price, copies);
        this.orderQty = orderQty;
        this.currIssue = currIssue;
    }
    
    public void recvNewIssue(String newIssue) {
        setCopies(getCopies() + orderQty);
        currIssue = newIssue;
    }

    /**
     * @return the orderQty
     */
    public Integer getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(Integer orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the currIssue
     */
    public String getCurrIssue() {
        return currIssue;
    }

    /**
     * @param currIssue the currIssue to set
     */
    public void setCurrIssue(String currIssue) {
        this.currIssue = currIssue;
    }
    
}