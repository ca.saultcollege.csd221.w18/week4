/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.InterfaceExperiment;

/**
 * This interface demonstrates multiple inheritance in interfaces!
 * 
 * @author rod
 */
public interface CanSingSong extends CanMakeMelody, CanSayWords {
    // This interface extends TWO other interfaces.
    // This kind of inheritance hierarchy is not possible with regular classes
    void singSong();
}
