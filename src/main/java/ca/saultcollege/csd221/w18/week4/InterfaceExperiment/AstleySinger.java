/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.InterfaceExperiment;

/**
 *
 * @author rod
 */
public class AstleySinger extends Singer {
    
    // Use Override to denote that we're implementing the missing interface
    // method that Singer left abstract
    @Override 
    public void sayWords() {
        System.out.println("Good evening, Cleveland!");
    }
    
    @Override
    public void singSong() {
        System.out.println("Never gonna give you up!");
        System.out.println("Never gonna let you down!");
    }
}
