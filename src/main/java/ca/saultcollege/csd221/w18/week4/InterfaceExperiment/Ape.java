/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.InterfaceExperiment;

/**
 *
 * @author rod
 */
public class Ape implements CanMakeNoise {
    
    // Override denotes that an implementation of an interface method
    @Override
    public void makeNoise() {
        System.out.println("Oook ook!");
    }
}
