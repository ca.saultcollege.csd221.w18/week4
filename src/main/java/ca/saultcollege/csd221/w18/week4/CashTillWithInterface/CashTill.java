/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.CashTillWithInterface;

import ca.saultcollege.csd221.w18.week4.CashTillWithAbstractClass.*;

/**
 *
 * @author rod
 */
public class CashTill {
    
    private Double runningTotal;
    
    public CashTill() {
        runningTotal = 0.0;
    }
    
    // Now, ANY class that implements the Saleable interface can be sold by
    // CashTill, not just classes that extend Publication.
    public void sellItem(Saleable item) {
        runningTotal += item.getPrice();
        
        item.sellCopy();
        
        System.out.println("Sold " + item + " @ " + item.getPrice());
        System.out.println("Subtotal = " + runningTotal);
    }
    
    public void showTotal() {
        System.out.println("GRAND TOTAL: " + runningTotal);
    }
    
    
    // This is a naive implementation of saleType.  Note the use of instanceof.
    // The method will need to be changed any time a new Saleable type is
    // created.  We have reduced the program's extendability!
    public void saleType(Saleable item) {
        
        if ( item instanceof Publication ) {
            System.out.println("This is a publication");
        } else if ( item instanceof Ticket ) {
            System.out.println("This is a ticket");
        } else {
            System.out.println("This is an unknown sale type");
        }
    }
    
    
    // Instead of the method above, why not add a new behaviour to the interface
    // that we can use?
    // Exercise: add the describeSelf method to the Saleable interface and 
    // make the necessary changes to any classes that require it.
//    public void saleType(Saleable item) {
//        item.describeSelf(); 
//    }
}
