/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.CashTillNoPolymorphism;

/**
 *
 * @author rod
 */
public class DiscMag extends Magazine {
    
    public DiscMag(String title, Integer price, int orderQty, String currIssue, Integer copies) {
        super(title, price, orderQty, currIssue, copies);
    }
    
    public void recvNewIssue(String newIssue) {
        super.recvNewIssue(newIssue);
        
        System.out.println("Check discs attached to this magazine");
    }
}
