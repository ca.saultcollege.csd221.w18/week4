/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.InterfaceExperiment;

/**
 *
 * @author rod
 */
public interface CanMakeNoise {
    
    // Interfaces can specify a default method
    // BUT the method cannot rely on any internal state (there is none from
    // within an interface)
    default void makeNoise() {
        System.out.println("*grunt*");
    }
}
