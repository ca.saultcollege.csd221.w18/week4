/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.CashTillWithInterface;

/**
 *
 * @author rod
 */
public class Ticket implements Saleable {
    
    private Integer price;
    
    public Ticket(Integer price) {
        this.price = price;
    }
    
    public Integer getPrice() {
        return price;
    }
    
    public void sellCopy() {
        System.out.println("**************************************"); 
        System.out.println(" TICKET VOUCHER "); 
        System.out.println(toString()); 
        System.out.println("**************************************"); 
        System.out.println();
    }
    
    
}
