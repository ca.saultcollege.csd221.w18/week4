/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.OverridingExperiment;

/**
 *
 * @author rod
 */
public class Sub extends Super {
    
    @Override
    public void theMethod() {
        
        // What happens when this line is uncommented?
        // super.theMethod();
        
        // What happens when this line is uncommented?
        // theMethod();
        
        System.out.println("Hello from Sub");
    }
    
    public static void main(String[] args) {
        Super s1 = new Super();
        s1.theMethod();
        
        Sub s2 = new Sub();
        s2.theMethod();
    }
}
