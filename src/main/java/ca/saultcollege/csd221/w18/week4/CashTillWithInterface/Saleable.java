/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week4.CashTillWithInterface;

/**
 *
 * @author rod
 */
public interface Saleable {
    
    // An interface can ONLY have methods (NO attributes)
    
    // An interface CANNOT have a constructor 
    // (You can't instantiate interfaces directly; you instantiate classes that
    // implement interfaces)
    
    // Interface methods are public and abstract by default
    Integer getPrice();
    
    // As of Java 8, an interface CAN provide a 'default' method implementation
    // HOWEVER, such methods cannot rely on internal state of the class because
    // the interface knows nothing about internal state (it has no attributes)
    // Classes that implement interfaces with default methods don't have to 
    // override them (but they may)
    default void sellCopy() {
        System.out.println("This is a default method");
    }
    
}
